import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Users {
    String name;
    String email;
    Date birthdate;
    Users(String name,String email,String birthdate){
        this.name = name;
        this.email=email;
        DateFormat f = new SimpleDateFormat("dd.MM.yyyy");
        try{
            this.birthdate= f.parse(birthdate);
        }catch(ParseException e){
            e.printStackTrace();
        }
    }
}
class Admin extends Users{
    double salary;
    String password;
    Admin(String name,String email,String birthdate,Double salary,String password){
        super(name,email,birthdate);
        this.salary = salary;
        this.password=password;
    }
}
class Tech extends Users{
    double salary;
    boolean isSeniorTechnician ;
    Tech(String name,String email,String birthdate,Double salary,int isSeniorTechnician ){
        super(name,email,birthdate);
        this.salary = salary;
        this.isSeniorTechnician=(isSeniorTechnician==1);
    }
}
class Customer extends Users{
    double balance;
    String password;
    String status;
    public int customerId;
    private static int raisedinteger=1;
    Customer(String name,String email,String birthdate,Double balance,String password){
        super(name,email,birthdate);
        this.balance = balance;
        this.password=password;
        this.customerId = this.raisedinteger;
        this.raisedinteger++;
    }

}
