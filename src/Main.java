import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    public static ArrayList<Users> users = new ArrayList<Users>();
    public static ArrayList<Items> items = new ArrayList<Items>();
    public static ArrayList<Campaign> campaigns = new ArrayList<Campaign>();
    public static ArrayList<Cart> carts = new ArrayList<Cart>();
    public static String dotdate(Date birthdate){
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        String reportDate = df.format(birthdate);
        return reportDate;
    }
    public static String slashdate(Date birthdate){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String reportDate = df.format(birthdate);
        return reportDate;
    }
    public static void addcostumer(String name,String email,String birthdate,Double balance,String password){
        users.add(new Customer( name, email, birthdate, balance, password));
    }
    public static void addtech(String name,String email,String birthdate,Double salary,int isSeniorTechnician ){
        users.add(new Tech( name, email, birthdate, salary, isSeniorTechnician));

    }
    public static void addadmin(String name,String email,String birthdate,Double salary,String password){
        users.add(new Admin( name, email, birthdate, salary, password));
    }
    public static int authlevel(String name){
        int level = 0;
        for(Users authuser : users){
            if(authuser.name.equals(name) && authuser.getClass().getName().equals("main.Admin")){
                level = 3;
            }else
            if(authuser.name.equals(name) && authuser.getClass().getName().equals("main.Tech")){
                level = 2;
            }else
            if(authuser.name.equals(name) && authuser.getClass().getName().equals("main.Customer")){
                level = 1;
            }
        }
        return level;
    }
    public static void auth_adduser(String command[]){
        if(authlevel(command[1])==3){
            if(command[0].equals("ADDADMIN")){
                addadmin(command[2],command[3],command[4],Double.parseDouble(command[5]),command[6]);
            }
            if(command[0].equals("ADDTECH")){
                addtech(command[2],command[3],command[4],Double.parseDouble(command[5]),Integer.parseInt(command[6]));
            }
            if(command[0].equals("ADDCUSTOMER")){
                addcostumer(command[2],command[3],command[4],Double.parseDouble(command[5]), command[6]);
            }
        }else{
            System.out.printf("No admin person named %s exists!\n",command[1]);
        }
    }
    public static void showuser(String command[]){
        int auth_level = authlevel(command[1]);
        if((command[0].equals("SHOWCUSTOMER") || command[0].equals("SHOWCUSTOMERS"))){
            if(auth_level==3){
                if(command[0].equals("SHOWCUSTOMER")){
                    for(Users finduser : users){
                        if(finduser.getClass().getName().equals("main.Customer")){
                            int customerId =Integer.parseInt(command[2]);
                            Customer foundcustomer = (Customer)finduser;
                            foundcustomer.status = userstatus(totalorder(foundcustomer.customerId));
                            if(foundcustomer.customerId==customerId){
                                System.out.printf("Customer name: %s	ID: %s	e-mail: %s	 Date of Birth: %s	Status: %s\n",foundcustomer.name,foundcustomer.customerId,foundcustomer.email,foundcustomer.birthdate,foundcustomer.status);
                            }
                        }
                    }
                }
                if(command[0].equals("SHOWCUSTOMERS")){
                    for(Users finduser : users){
                        if(finduser.getClass().getName().equals("main.Customer")){
                            Customer foundcustomer = (Customer)finduser;
                            foundcustomer.status = userstatus(totalorder(foundcustomer.customerId));
                            System.out.printf("Customer name: %s	ID: %s	e-mail: %s	 Date of Birth: %s	Status: %s\n",foundcustomer.name,foundcustomer.customerId,foundcustomer.email,foundcustomer.birthdate,foundcustomer.status);

                        }
                    }
                }
            }else{
                System.out.printf("No admin person named %s exists!\n", command[1]);
            }
        }else{
            if(command[0].equals("SHOWADMININFO")){
                boolean founded=false;
                for(Users finduser : users){
                    if(finduser.getClass().getName().equals("main.Admin") && finduser.name.equals(command[1]) && !founded){
                        System.out.printf(
                                "----------- Admin info -----------\n"+
                                        "Admin name: %s\n"+
                                        "Admin e-mail: %s\n"+
                                        "Admin  date of birth: %s\n", finduser.name,finduser.email,dotdate(finduser.birthdate));
                        founded=true;

                    }
                }
                if(!founded){
                    System.out.printf("No admin person named %s exists!\n",command[1]);
                }
            }
        }
    }
    public static void createcampaign(String adminName,String startdate, String enddate,String itemcategory,int rate){
        int authlevel = authlevel(adminName);
        if(authlevel==3){
            if(rate>50){
                System.out.println("Campaign was not created. Discount rate exceeds maximum rate of 50%.");
            }else{
                campaigns.add(new Campaign(adminName,startdate,enddate,itemcategory,rate));
            }
        }else{
            System.out.printf("No admin named Alper exists!", adminName);
        }
    }
    public static void showcampaigns(int customerId){
        boolean customerfounded = false;
        Date today = new Date();
        int countcamp = 0;
        for(Users findid : users){
            if(findid.getClass().getName().equals("main.Customer"))
            {
                Customer founded = (Customer)findid;
                if(founded.customerId==customerId)
                    customerfounded = true;
            }
        }
        if(customerfounded){
            if(campaigns.size()>0){


                for(Campaign camp : campaigns){
                    if(today.compareTo(camp.enddate)>0){

                    }else{
                        if(countcamp == 0)
                            System.out.println("Active campaigns:");
                        System.out.println(camp.rate+"% sale of "+camp.itemtype.toUpperCase()+" until "+slashdate(camp.enddate));
                        countcamp++;
                    }
                }
                if(countcamp==0){
                    System.out.println("No campaign has been created so far!");
                }
            }else{
                System.out.println("No campaign has been created so far!");
            }
        }else{
            System.out.printf("No customer with ID number %s exists!\n", customerId);
        }

    }
    public static double campaigndiscount(int itemId){

        double returncost=0;
        String itemtype="";
        int discount = 0;
        Date today = new Date();
        for(Items item : items){
            if(item.itemid==itemId){
                returncost= item.cost;
                itemtype = item.getClass().getName().toString().replace("main.", "");
            }
        }
        for(Campaign camp : campaigns){
            if(camp.itemtype.toLowerCase().equals(itemtype.toLowerCase())){
                if(today.compareTo(camp.enddate)>0){

                }else{

                    discount = camp.rate;
                }
            }
        }
        return returncost - (returncost*discount/100);
    }
    public static void addtocart(int customerId,int itemId){
        boolean foundeduser = false;
        boolean foundeditem = false;
        boolean itemstatus=true;
        double cost = 0;
        String itemtype ="";
        for(Users finduser : users){
            if(finduser.getClass().getName().equals("main.Customer")){
                Customer foundcustomer = (Customer)finduser;
                if(foundcustomer.customerId==customerId)
                    foundeduser=true;
            }
        }
        for(Items finditem : items){
            if(finditem.itemid==itemId){
                foundeditem=true;
                itemtype = finditem.getClass().getName();
                cost = finditem.cost;
                itemstatus=finditem.status;
            }

        }
        if(foundeduser && foundeditem){
            if(itemstatus){
                carts.add(new Cart(customerId,itemId,1,campaigndiscount(itemId)));
                System.out.printf("The item %s has been successfully added to your cart.\n", itemtype.replace("main.", "").toUpperCase());
            }else{
                System.out.println("We are sorry. The item is temporarily unavailable.\n");
            }
        }else{
            if(!foundeduser)
                System.out.printf("No customer with ID number %s exists!\n",customerId);
            else
            if(!foundeditem)
                System.out.printf("Invalid item ID\n", itemId);
        }
    }
    public static void depositmoney(int customerId,double loadAmount){
        boolean foundeduser = false;
        for(Users finduser : users){
            if(finduser.getClass().getName().equals("main.Customer")){
                Customer foundcustomer = (Customer)finduser;
                if(foundcustomer.customerId==customerId){
                    foundcustomer.balance = foundcustomer.balance+loadAmount;
                    int usersindex = users.indexOf(finduser);
                    users.set(usersindex,foundcustomer);
                    foundeduser=true;
                }
            }
        }
        if(!foundeduser){
            System.out.printf("No customer with ID number %s exists!\n",customerId);
        }

    }
    public static void emptycart(int customerId){
        boolean foundeduser = false;
        for(Users finduser : users){
            if(finduser.getClass().getName().equals("main.Customer")){
                Customer foundcustomer = (Customer)finduser;
                if(foundcustomer.customerId==customerId){
                    foundeduser = true;
                }
            }
        }
        if(!foundeduser){
            System.out.printf("No customer with ID number %s exists!\n",customerId);
        }else{
            System.out.println("The cart has been emptied.");
        }
    }
    public static void showitemslowonstock(String adminName,int maxStockAmount){
        Map<String,Integer> inventory = new HashMap<String,Integer>();
        inventory.put("Book",0);
        inventory.put("Cddvd",0);
        inventory.put("Desktop",0);
        inventory.put("Laptop",0);
        inventory.put("Tablet",0);
        inventory.put("Tv",0);
        inventory.put("SmartPhone",0);
        inventory.put("Haircare",0);
        inventory.put("Perfume",0);
        inventory.put("Skincare",0);
        int authlevel = authlevel(adminName);
        if(authlevel==3 || authlevel==2){
            for(Items item : items){
                String itemtype = item.getClass().getName().replace("main.","");
                boolean orderstatus = false;
                for(Cart cart : carts){
                    if(cart.itemid==item.itemid && cart.status==3)
                        orderstatus=true;
                }
                if(!orderstatus)
                    inventory.computeIfPresent(itemtype.trim(),(k,v)->v+1);
            }
            for(Map.Entry entry : inventory.entrySet()){
                int value = Integer.parseInt(entry.getValue().toString());
                if(value<=maxStockAmount){
                    System.out.println(entry.getKey() + " : " + entry.getValue());
                }
            }

        }else{
            System.out.printf("No admin or technician person named %s exists!",adminName);
        }
    }
    public static void chpass(int customerId,String oldPassword,String newPassword){
        boolean foundeduser = false;
        for(Users finduser : users){
            if(finduser.getClass().getName().equals("main.Customer")){
                Customer foundcustomer = (Customer)finduser;
                if(foundcustomer.customerId==customerId){
                    if(foundcustomer.password.equals(oldPassword)){
                        foundeduser=true;
                        foundcustomer.password = newPassword;
                        int usersindex = users.indexOf(finduser);
                        users.set(usersindex,foundcustomer);
                        System.out.println("The password has been successfully changed.");
                    }else
                    {
                        System.out.println("The given password does not match the current password. Please try again.");
                    }
                }
            }
        }
        if(!foundeduser){
            System.out.printf("No customer with ID number %s exists!\n",customerId);
        }
    }
    public static void listitem(String adminName,String[] types){
        int authlevel = authlevel(adminName);
        if(authlevel==3 || authlevel==2){
            for(Items item : items){
                String itemtype = item.getClass().getName().replace("main.","");
                if(Arrays.asList(types).contains(itemtype.toLowerCase())){
                    if(itemtype.equals("Book")){
                        Book item2 = (Book)item;
                        System.out.println("-----------------------"
                                + "\nType: "+itemtype.toUpperCase()
                                + "\nItem ID: "+item2.itemid
                                + "\nPrice: "+item2.cost+" $"
                                + "\nRelease Date: "+item2.releasedate
                                + "\nTitle: "+item2.covertitle
                                + "\nPublisher: "+item2.publishername
                                + "\nAuthor: "+String.join(",", item2.authors)
                                + "\nPage: "+item2.pagenumber+" pages");
                    }else
                    if(itemtype.equals("Cddvd")){
                        Cddvd item2 = (Cddvd)item;
                        System.out.println("-----------------------"
                                + "\nType: "+itemtype.toUpperCase()
                                + "\nItem ID: "+item2.itemid
                                + "\nPrice: "+item2.cost
                                + "\nRelease Date: "+item2.releasedate
                                + "\nTitle: "+item2.covertitle
                                + "\nSongs: "+String.join(",", item2.songs)
                                + "\nComposer: "+item2.composer);
                    }else
                    if(itemtype.equals("Desktop")){
                        Desktop item2 = (Desktop)item;
                        System.out.println("-----------------------"
                                + "\nType: "+itemtype.toUpperCase()
                                + "\nItem ID: "+item2.itemid
                                + "\nPrice: "+item2.cost+" $"
                                + "\nManufacturer: "+item2.manufacturer
                                + "\nBrand: "+item2.brand
                                + "\nMax Volt: "+item2.maxVolt +" V."
                                + "\nMax Watt: "+item2.maxWatt +" W."
                                + "\nOperating System: "+item2.operatingSystem
                                + "\nCPU Type: "+item2.CPUType
                                + "\nRAM Capacity:  "+item2.ramCapacity + " GB."
                                + "\nHDD Capacity: "+item2.HDDCapacity + " GB."
                                + "\nBox Color: "+item2.boxcolor
                        );
                    }
                    else
                    if(itemtype.equals("Laptop")){
                        Laptop item2 = (Laptop)item;
                        String hdmisupport = item2.hdmisupport ? "Yes": "No";
                        System.out.println("-----------------------"
                                + "\nType: "+itemtype.toUpperCase()
                                + "\nItem ID: "+item2.itemid
                                + "\nPrice: "+item2.cost+" $"
                                + "\nManufacturer: "+item2.manufacturer
                                + "\nBrand: "+item2.brand
                                + "\nMax Volt: "+item2.maxVolt +" V."
                                + "\nMax Watt: "+item2.maxWatt +" W."
                                + "\nOperating System: "+item2.operatingSystem
                                + "\nCPU Type: "+item2.CPUType
                                + "\nRAM Capacity:  "+item2.ramCapacity + " GB."
                                + "\nHDD Capacity: "+item2.HDDCapacity + " GB."
                                + "\nHDMI Support: "+hdmisupport
                        );
                    }
                    else
                    if(itemtype.equals("Tablet")){
                        Tablet item2 = (Tablet)item;
                        System.out.println("-----------------------"
                                + "\nType: "+itemtype.toUpperCase()
                                + "\nItem ID: "+item2.itemid
                                + "\nPrice: "+item2.cost+" $"
                                + "\nManufacturer: "+item2.manufacturer
                                + "\nBrand: "+item2.brand
                                + "\nMax Volt: "+item2.maxVolt +" V."
                                + "\nMax Watt: "+item2.maxWatt +" W."
                                + "\nOperating System: "+item2.operatingSystem
                                + "\nCPU Type: "+item2.CPUType
                                + "\nRAM Capacity:  "+item2.ramCapacity + " GB."
                                + "\nHDD Capacity: "+item2.HDDCapacity + " GB."
                                + "\nDimension: "+item2.dimension+" in."
                        );
                    }
                    else
                    if(itemtype.equals("Tv")){
                        Tv item2 = (Tv)item;
                        System.out.println("-----------------------"
                                + "\nType: "+itemtype.toUpperCase()
                                + "\nItem ID: "+item2.itemid
                                + "\nPrice: "+item2.cost+" $"
                                + "\nManufacturer: "+item2.manufacturer
                                + "\nBrand: "+item2.brand
                                + "\nMax Volt: "+item2.maxVolt +" V."
                                + "\nMax Watt: "+item2.maxWatt +" W."
                                + "\nScreen size: "+item2.screensize+"\""
                        );
                    }
                    else
                    if(itemtype.equals("SmartPhone")){
                        SmartPhone item2 = (SmartPhone)item;
                        System.out.println("-----------------------"
                                + "\nType: "+itemtype.toUpperCase()
                                + "\nItem ID: "+item2.itemid
                                + "\nPrice: "+item2.cost+" $"
                                + "\nManufacturer: "+item2.manufacturer
                                + "\nBrand: "+item2.brand
                                + "\nMax Volt: "+item2.maxVolt +" V."
                                + "\nMax Watt: "+item2.maxWatt +" W."
                                + "\nScreen Type: "+item2.screentype
                        );
                    }
                    else
                    if(itemtype.equals("Haircare")){
                        Haircare item2 = (Haircare)item;
                        String organic = item2.organic ? "Yes" : "No";
                        String isMedicated = item2.isMedicated ? "Yes" : "No";
                        System.out.println("-----------------------"
                                + "\nType: "+itemtype
                                + "\nItem ID: "+item2.itemid
                                + "\nPrice: "+item2.cost+" $"
                                + "\nManufacturer: "+item2.manufacturer
                                + "\nBrand: "+item2.brand+" $"
                                + "\nOrganic: "+organic
                                + "\nExpiration Date: "+item2.expirationdate
                                + "\nWeight: "+item2.weight +" g."
                                + "\nMedicated: "+isMedicated
                        );
                    }
                    else
                    if(itemtype.equals("Perfume")){
                        Perfume item2 = (Perfume)item;
                        String organic = item2.organic ? "Yes" : "No";
                        System.out.println("-----------------------"
                                + "\nType: "+itemtype
                                + "\nItem ID: "+item2.itemid
                                + "\nPrice: "+item2.cost+" $"
                                + "\nManufacturer: "+item2.manufacturer
                                + "\nBrand: "+item2.brand+" $"
                                + "\nOrganic: "+organic
                                + "\nExpiration Date: "+item2.expirationdate
                                + "\nWeight: "+item2.weight +" g."
                                + "\nLasting Duration: "+item2.lastduration+" min."
                        );
                    }

                    else
                    if(itemtype.equals("Skincare")){
                        Skincare item2 = (Skincare)item;
                        String babySensitive = item2.babySensitive ? "Yes" : "No";
                        String organic = item2.organic ? "Yes" : "No";
                        System.out.println("-----------------------"
                                + "\nType: "+itemtype.toUpperCase()
                                + "\nItem ID: "+item2.itemid
                                + "\nPrice: "+item2.cost+" $"
                                + "\nManufacturer: "+item2.manufacturer
                                + "\nBrand: "+item2.brand+" $"
                                + "\nOrganic: "+organic
                                + "\nExpiration Date: "+item2.expirationdate
                                + "\nWeight: "+item2.weight +" g."
                                + "\nBaby Sensitive: "+babySensitive
                        );
                    }
                }
            }

        }else{
            System.out.printf("No admin or technician person named %s exists!",adminName);
        }
    }
    public static void usertxt(String args){
        try{
            FileReader fileReader = new FileReader(args);
            BufferedReader br = new BufferedReader(fileReader);
            String str;
            while ((str = br.readLine()) != null)
            {

                String ulist[] = str.split("\t");
                if(ulist[0].equals("ADMIN")){
                    addadmin(ulist[1],ulist[2],ulist[3],Double.parseDouble(ulist[4]),ulist[5]);
                }
                if(ulist[0].equals("TECH")){
                    addtech(ulist[1],ulist[2],ulist[3],Double.parseDouble(ulist[4]),Integer.parseInt(ulist[5]));
                }
                if(ulist[0].equals("CUSTOMER")){
                    addcostumer(ulist[1],ulist[2],ulist[3],Double.parseDouble(ulist[4]), ulist[5]);
                }

            }
            br.close();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
    public static void additem(String isplit[]){

        if(isplit[0].equals("BOOK")){
            items.add(new Book(Integer.parseInt(isplit[2]),isplit[3],Double.parseDouble(isplit[1]) ,isplit[4],isplit[5],Integer.parseInt(isplit[6])));
        }else
        if(isplit[0].equals("CDDVD")){
            items.add(new Cddvd(Integer.parseInt(isplit[2]),isplit[3],Double.parseDouble(isplit[1]),isplit[4],isplit[5]));
        }else
        if(isplit[0].equals("DESKTOP")){
            items.add(new Desktop(isplit[1],isplit[3],Integer.parseInt(isplit[4]),Integer.parseInt(isplit[5]),Double.parseDouble(isplit[1]),isplit[6],isplit[7],Integer.parseInt(isplit[8]),Integer.parseInt(isplit[9]),isplit[10]));
        }else
        if(isplit[0].equals("LAPTOP")){
            items.add(new Laptop(isplit[1],isplit[3],Integer.parseInt(isplit[4]),Integer.parseInt(isplit[5]),Double.parseDouble(isplit[1]),isplit[6],isplit[7],Integer.parseInt(isplit[8]),Integer.parseInt(isplit[9]),Integer.parseInt(isplit[10])));
        }else
        if(isplit[0].equals("TABLET")){
            items.add(new Tablet(isplit[1],isplit[3],Integer.parseInt(isplit[4]),Integer.parseInt(isplit[5]),Double.parseDouble(isplit[1]),isplit[6],isplit[7],Integer.parseInt(isplit[8]),Integer.parseInt(isplit[9]),Integer.parseInt(isplit[10])));
        }else
        if(isplit[0].equals("TV")){
            items.add(new Tv(isplit[2],isplit[3],Integer.parseInt(isplit[4]),Integer.parseInt(isplit[5]),Double.parseDouble(isplit[1]),Integer.parseInt(isplit[6])));
        }else
        if(isplit[0].equals("SMARTPHONE")){
            items.add(new SmartPhone(isplit[2],isplit[3],Integer.parseInt(isplit[4]),Integer.parseInt(isplit[5]),Double.parseDouble(isplit[1]),isplit[6]));
        }else
        if(isplit[0].equals("HAIRCARE")){
            items.add(new Haircare(isplit[2],isplit[3],Integer.parseInt(isplit[5]),Integer.parseInt(isplit[6]),Integer.parseInt(isplit[4]),Double.parseDouble(isplit[1]),Integer.parseInt(isplit[7])));
        }else
        if(isplit[0].equals("PERFUME")){
            items.add(new Perfume(isplit[2],isplit[3],Integer.parseInt(isplit[5]),Integer.parseInt(isplit[6]),Integer.parseInt(isplit[4]),Double.parseDouble(isplit[1]),Integer.parseInt(isplit[7])));
        }else
        if(isplit[0].equals("SKINCARE")){
            items.add(new Skincare(isplit[2],isplit[3],Integer.parseInt(isplit[5]),Integer.parseInt(isplit[6]),Integer.parseInt(isplit[4]),Double.parseDouble(isplit[1]),Integer.parseInt(isplit[7])));
        }else{
            System.out.printf("No item type %s found", isplit[0]);
        }

    }
    public static void commandadditem(String technicianName,String itemcommand){
        if(authlevel(technicianName)==2){
            additem(itemcommand.split(":"));
        }else
        {
            System.out.printf("No technician person named %s exists!",technicianName);
        }

    }
    public static String userstatus(double orders){
        String status="CLASSIC";
        if(orders>1000)
            status = "SILVER";
        if(orders>5000)
            status = "GOLDEN";
        return status;
    }
    public static double totalorder(int customerId){
        double totalorder = 0;
        for (int j = 0; j < carts.size(); j++) {
            if(carts.get(j).costumerid==customerId && carts.get(j).status==3)
                totalorder += carts.get(j).cost;
        }
        return totalorder;
    }
    public static int discount(String status){
        int discount = 0;
        if(status=="SILVER")
            discount = 10;
        if(status=="GOLDEN")
            discount = 15;
        return discount;
    }
    public static double discountvip(double totalcost,String status){
        double returncost = totalcost;
        if(status=="SILVER")
            returncost = totalcost - (totalcost*10/100);
        if(status=="GOLDEN")
            returncost =  totalcost - (totalcost*15/100);
        return returncost;
    }
    public static void order(int customerId,String customerPassword){
        boolean foundeduser = false;
        double totalcost =0;
        int totalitem = 0;
        double customertotalorder = totalorder(customerId);
        for(Users finduser : users){
            if(finduser.getClass().getName().equals("main.Customer")){
                Customer foundcustomer = (Customer)finduser;
                String customerstatus = userstatus(customertotalorder);
                if(foundcustomer.customerId==customerId){
                    foundeduser = true;
                    if(foundcustomer.password.equals(customerPassword)){
                        for (int j = 0; j < carts.size(); j++) {
                            if(carts.get(j).costumerid==customerId && carts.get(j).status==1){
                                totalcost += carts.get(j).cost;
                                totalitem++;
                            }

                        }
                        if(totalcost==0){
                            System.out.println("You should add some items to your cart before order request!");
                        }else{
                            totalcost = discountvip(totalcost,customerstatus);
                            if(foundcustomer.balance>=totalcost){
                                for (int js = 0; js < carts.size(); js++) {
                                    if(carts.get(js).costumerid==customerId && carts.get(js).status==1){
                                        carts.get(js).status=3;
                                        carts.get(js).orderdate=new Date();
                                        for (int ja = 0; ja < items.size(); ja++) {
                                            if(items.get(ja).itemid==carts.get(js).itemid)
                                                //items.remove(ja);
                                                items.get(ja).status = false;
                                        }
                                    }
                                }
                                foundcustomer.balance=foundcustomer.balance - totalcost;
                                int usersindex = users.indexOf(finduser);
                                users.set(usersindex,foundcustomer);

                                System.out.println("Done! Your order will be delivered as soon as possible. Thank you!");
                                String newstatus = userstatus(customertotalorder+totalcost);
                                double lastotal = customertotalorder+totalcost;
                                if(customerstatus!=newstatus){
                                    System.out.println("Congratulations! You have been upgraded to a "+newstatus+" MEMBER! You have earned a discount of "+discount(newstatus)+"% on all purchases.");
                                }
                                if(newstatus=="CLASSIC" && lastotal<1000)
                                    System.out.println("You need to spend "+(1000-lastotal)+" more TL to become a SILVER MEMBER.");
                                if(newstatus=="SILVER" && lastotal<5000)
                                    System.out.println("You need to spend "+(5000-lastotal)+" more TL to become a GOLDEN MEMBER.");
                                break;


                            }else{
                                System.out.println("Order could not be placed. Insufficient funds.");
                            }
                        }
                    }else{
                        System.out.println("Order Could not be placed. Invalid password.");
                    }
                }
            }
        }
        if(!foundeduser){
            System.out.printf("No customer with ID number %s exists!\n",customerId);
        }
    }
    public static void showvip(String userName){
        int authlevel = authlevel(userName);
        if(authlevel==3 || authlevel==2){
            for(Users finduser : users){
                if(finduser.getClass().getName().equals("main.Customer")){
                    Customer foundcustomer = (Customer)finduser;
                    String status = userstatus(totalorder(foundcustomer.customerId));
                    if(status=="GOLDEN" || status=="SILVER"){
                        System.out.println("Customer name: "+foundcustomer.name+"	ID: "+foundcustomer.customerId+"	e-mail: "+foundcustomer.email+"	 Date of Birth: "+foundcustomer.birthdate+"	Status: "+status);
                    }
                }
            }
        }else{
            System.out.printf("No admin or technician person named %s exists!",userName);
        }
    }
    public static void showorders(String userName){
        boolean detecttech = false;
        boolean detectsenior = false;
        String printvalue="";
        for(Users finduser : users){
            if(finduser.getClass().getName().equals("main.Tech")){
                Tech technician = (Tech)finduser;
                if(technician.name.equals(userName)){
                    detecttech=true;
                }
                if(technician.name.equals(userName) && technician.isSeniorTechnician){
                    detectsenior = true;

                }
            }else
            if(finduser.getClass().getName().equals("main.Customer")){
                Customer customer = (Customer)finduser;
                double totalcost = 0;
                int totalitems=0;
                Date orderdate=new Date();
                for(Cart cart : carts){
                    if(cart.costumerid==customer.customerId && cart.status==3){
                        totalcost += cart.cost;
                        totalitems++;
                        orderdate = cart.orderdate;
                    }
                }
                if(totalitems>0)
                    printvalue +="Order Date: "+ orderdate +" Customer ID:" + customer.customerId +" Total Cost: " + totalcost + " Number of purchased items: "+totalitems+"\n";
            }
        }
        if(!detectsenior && !detecttech)
        {
            System.out.printf("No technician person named %s exists!",userName);
        }else
        if(!detecttech)
        {
            System.out.printf("%s is not authorized to display orders!", userName);
        }else{
            System.out.println("Order History:");
            System.out.println(printvalue);
        }
    }
    public static void itemstxt(String args){
        try{
            FileReader fileReader = new FileReader(args);
            BufferedReader br = new BufferedReader(fileReader);
            String str;
            while ((str = br.readLine()) != null)
            {

                String isplit[] = str.split("\t");

                additem(isplit);
            }
            br.close();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
    public static void main (String [] args)// this is main function for all operations
    {

        usertxt(args[0]);
        itemstxt(args[1]);

        try{
            FileReader fileReader = new FileReader(args[2]);
            BufferedReader br = new BufferedReader(fileReader);
            String str;
            while ((str = br.readLine()) != null)
            {
                System.out.printf("\nCOMMAND TEXT: <%s>\n\n",str);
                String commandsplit[] = str.split("	");
                if(commandsplit[0].equals("ADDCUSTOMER") || commandsplit[0].equals("ADDTECH") || commandsplit[0].equals("ADDADMIN")){
                    auth_adduser(commandsplit);
                }
                if(commandsplit[0].equals("SHOWCUSTOMER") || commandsplit[0].equals("SHOWCUSTOMERS") || commandsplit[0].equals("SHOWADMININFO")){
                    showuser(commandsplit);
                }
                if(commandsplit[0].equals("ADDTOCART")){
                    addtocart(Integer.parseInt(commandsplit[1]),Integer.parseInt(commandsplit[2]));
                }
                if(commandsplit[0].equals("CREATECAMPAIGN")){
                    createcampaign(commandsplit[1],commandsplit[2],commandsplit[3],commandsplit[4],Integer.parseInt(commandsplit[5]));
                }
                if(commandsplit[0].equals("SHOWCAMPAIGNS")){
                    showcampaigns(Integer.parseInt(commandsplit[1]));
                }
                if(commandsplit[0].equals("DEPOSITMONEY")){
                    depositmoney(Integer.parseInt(commandsplit[1]),Double.parseDouble(commandsplit[2]));
                }
                if(commandsplit[0].equals("EMPTYCART")){
                    emptycart(Integer.parseInt(commandsplit[1]));
                }
                if(commandsplit[0].equals("SHOWITEMSLOWONSTOCK")){
                    showitemslowonstock(commandsplit[1],Integer.parseInt(commandsplit[2]));
                }
                if(commandsplit[0].equals("CHPASS")){
                    chpass(Integer.parseInt(commandsplit[1]),commandsplit[2],commandsplit[3]);
                }
                if(commandsplit[0].equals("LISTITEM")){
                    listitem(commandsplit[1],new String[]{"book","cddvd","desktop","laptop","tablet","tv","smartphone","haircare","perfume","skincare"});
                }
                if(commandsplit[0].equals("DISPITEMSOF")){
                    listitem(commandsplit[1],commandsplit[2].toLowerCase().split(":"));
                }
                if(commandsplit[0].equals("ADDITEM")){
                    commandadditem(commandsplit[1],commandsplit[2]);
                }
                if(commandsplit[0].equals("ORDER")){
                    order(Integer.parseInt(commandsplit[1]),commandsplit[2]);
                }
                if(commandsplit[0].equals("SHOWVIP")){
                    showvip(commandsplit[1]);
                }
                if(commandsplit[0].equals("SHOWORDERS")){
                    showorders(commandsplit[1]);
                }
            }
            br.close();
        }catch(IOException e){

        }
    }
}
