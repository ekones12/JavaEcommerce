
public class Items {
    public int itemid;
    private static int raisedinteger=1;
    double cost;
    boolean status;
    Items(double cost){
        this.cost = cost;
        this.itemid = this.raisedinteger;
        this.raisedinteger++;
        this.status=true;
    }
}
class Cosmetic extends Items{
    String manufacturer;
    String brand;
    int expirationdate;
    int weight;
    boolean organic;
    Cosmetic(String manufacturer,String brand,int expirationdate,int weight,int organic,double cost){
        super(cost);
        this.manufacturer = manufacturer;
        this.brand = brand;
        this.expirationdate = expirationdate;
        this.weight = weight;
        this.organic = (organic==1);
    }

}
class Perfume extends Cosmetic{
    int lastduration;
    Perfume(String manufacturer,String brand,int expirationdate,int weight,int organic,double cost,int lastduration){
        super( manufacturer, brand, expirationdate, weight, organic, cost);
        this.lastduration = lastduration;
    }
}
class Haircare extends Cosmetic{
    boolean isMedicated;
    Haircare(String manufacturer,String brand,int expirationdate,int weight,int organic,double cost,int isMedicated){
        super( manufacturer, brand, expirationdate, weight, organic, cost);
        this.isMedicated = (isMedicated==1);
    }
}

class Skincare extends Cosmetic{
    boolean babySensitive;
    Skincare(String manufacturer,String brand,int expirationdate,int weight,int organic,double cost,int babySensitive){
        super( manufacturer, brand, expirationdate, weight, organic, cost);
        this.babySensitive = (babySensitive==1);
    }
}
/*END-Cosmetics*/
/*Electroinics*/
class Electronic extends Items{
    String manufacturer;
    String brand;
    int maxVolt;
    int maxWatt;
    Electronic(String manufacturer,String brand,int maxVolt,int maxWatt,double cost){
        super(cost);
        this.manufacturer = manufacturer;
        this.maxVolt=maxVolt;
        this.maxWatt=maxWatt;
        this.brand = brand;
    }
}
/*Computers*/
class Computer extends Electronic{
    String operatingSystem;
    String CPUType;
    int ramCapacity;
    int HDDCapacity;
    Computer(String manufacturer,String brand,int maxVolt,int maxWatt,double cost,String operatingSystem,	String CPUType,	int ramCapacity,	int HDDCapacity){
        super(manufacturer, brand, maxVolt, maxWatt, cost);
        this.operatingSystem=operatingSystem;
        this.CPUType=CPUType;
        this.ramCapacity=ramCapacity;
        this.HDDCapacity=HDDCapacity;
    }
}
class Desktop extends Computer{
    String boxcolor;
    Desktop(String manufacturer,String brand,int maxVolt,int maxWatt,double cost,String operatingSystem,String CPUType,int ramCapacity,int HDDCapacity,String boxcolor){
        super(manufacturer, brand, maxVolt, maxWatt, cost,operatingSystem,CPUType,ramCapacity,HDDCapacity);
        this.boxcolor = boxcolor;
    }
}
class Laptop extends Computer{
    boolean hdmisupport;
    Laptop(String manufacturer,String brand,int maxVolt,int maxWatt,double cost,String operatingSystem,String CPUType,int ramCapacity,int HDDCapacity,int hdmisupport){
        super(manufacturer, brand, maxVolt, maxWatt, cost,operatingSystem,CPUType,ramCapacity,HDDCapacity);
        this.hdmisupport = (hdmisupport==1);
    }
}
class Tablet extends Computer{
    int dimension;
    Tablet(String manufacturer,String brand,int maxVolt,int maxWatt,double cost,String operatingSystem,String CPUType,int ramCapacity,int HDDCapacity,int dimension){
        super(manufacturer, brand, maxVolt, maxWatt, cost,operatingSystem,CPUType,ramCapacity,HDDCapacity);
        this.dimension = dimension;
    }
}
/*End-Computers*/
class Tv extends Electronic{

    int screensize;
    Tv(String manufacturer,String brand,int maxVolt,int maxWatt,double cost,int screensize){
        super(manufacturer, brand, maxVolt, maxWatt, cost);
        this.screensize=screensize;
    }
}
class SmartPhone extends Electronic{

    String screentype;
    SmartPhone(String manufacturer,String brand,int maxVolt,int maxWatt,double cost,String screentype){
        super(manufacturer, brand, maxVolt, maxWatt, cost);
        this.screentype=screentype;
    }
}
/*End-Electroinics*/
/*OfficeSupply*/
class Officesupply extends Items{
    int releasedate;
    String covertitle;
    Officesupply(int releasedate,String covertitle,double cost){
        super(cost);
        this.releasedate = releasedate;
        this.covertitle=covertitle;
    }
}
class Book extends Officesupply{
    String publishername;
    String authors[];
    int pagenumber;
    Book(int releasedate,String covertitle,double cost,String publishername,String authors,int pagenumber){
        super( releasedate, covertitle, cost);
        this.publishername=publishername;
        this.authors=authors.split(",");
        this.pagenumber=pagenumber;
    }
}
class Cddvd extends Officesupply{
    String composer;
    String songs[];
    Cddvd(int releasedate,String covertitle,double cost,String composer,String songs){
        super( releasedate, covertitle, cost);
        this.composer=composer;
        this.songs=songs.split(",");
    }
}
/*End-OfficeSupply*/



