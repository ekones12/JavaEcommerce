import java.util.Date;

public class Cart {
    int costumerid;
    int itemid;
    int status;
    double cost;
    Date orderdate;
    Cart(int costumerid,int itemid,int status,double cost){
        this.costumerid=costumerid;
        this.itemid=itemid;
        this.status=status;
        this.cost = cost;
    }
}
