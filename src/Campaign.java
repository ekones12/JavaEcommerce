import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Campaign {
    String adminname;
    Date startdate;
    Date enddate;
    String itemtype;
    int rate;
    Campaign(String adminname,String startdate,String enddate,String itemtype,int rate){
        this.adminname=adminname;
        this.itemtype=itemtype;
        this.rate=rate;
        DateFormat f = new SimpleDateFormat("dd.MM.yyyy");
        try{
            this.startdate=f.parse(startdate);
            this.enddate=f.parse(enddate);
        }catch(Exception e){

        }
    }
}
